package com.acg.app.ACGApplication.components;

import com.acg.app.ACGApplication.services.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableScheduling
public class ScheduledTasks {

    private final CarService carService;

    @Autowired
    public ScheduledTasks(CarService carService) {
        this.carService = carService;
    }

//    @Scheduled(cron = "0 0 2 ? * *")
//    private void updateCars() throws IOException {
//        carService.updateCars();
//    }
}
