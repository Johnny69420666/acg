package com.acg.app.ACGApplication.controllers;

import com.acg.app.ACGApplication.models.Image;
import com.acg.app.ACGApplication.services.ImageService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.w3c.dom.Text;

import java.util.List;

@RestController
@RequestMapping("/api/images")
public class ImageController {

    private final ImageService imageService;
    private static final Logger logger = LogManager.getLogger(ImageController.class);

    @Autowired
    public ImageController(ImageService imageService) {
        this.imageService = imageService;
    }

    @GetMapping(path = "/{uuid}", produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<byte[]> displayImageById(@PathVariable String uuid){
        Image profile = imageService.findImageById(uuid);
        if(profile != null){
            Byte[] profileData = profile.getData();
            byte[] bytes = new byte[profileData.length];

            int i=0;

            for (byte b: profileData){
                bytes[i++] = b;
            }

            return ResponseEntity
                    .ok()
                    .contentType(MediaType.IMAGE_JPEG)
                    .body(bytes);
        }
        return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(new byte[0]);
    }

    @GetMapping(path = "/car/{uuid}")
    public List<Image> getCarImagesByCarId(@PathVariable String uuid){
        return imageService.getCarImagesByCarId(uuid);
    }

    @PostMapping(path = "/delete")
    public void deleteImageById(@RequestBody String uuid){
        logger.info("Post request - deleteImageById, id:" + uuid);
        imageService.deleteImageById(uuid.substring(0,uuid.length()-1));
    }

}
