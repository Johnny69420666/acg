package com.acg.app.ACGApplication.services;

import javax.mail.MessagingException;

public interface MailService {

    void sendContactMail(String name, String surname, String phone,String email, String subject, String text) throws MessagingException;

    void sendAdminMail(String username, String password) throws MessagingException;
}
