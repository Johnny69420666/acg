package com.acg.app.ACGApplication.services.impl;

import com.acg.app.ACGApplication.models.Admin;
import com.acg.app.ACGApplication.repositories.AdminRepository;
import com.acg.app.ACGApplication.services.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AdminServiceImpl implements AdminService {

    private final AdminRepository adminRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public AdminServiceImpl(AdminRepository adminRepository, PasswordEncoder passwordEncoder) {
        this.adminRepository = adminRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public boolean authenticate(String username, String password){
        Optional<Admin> admin = adminRepository.findByUsername(username);
        return admin.isPresent() && passwordEncoder.matches(password, admin.get().getPassword());
    }

    @Override
    public Integer getExchangeRate(){
        Optional<Admin> admin = adminRepository.findByUsername("admin");
        if(admin.isPresent()){
            return admin.get().getExchangeRate();
        }
        else return 0;
    }
}
