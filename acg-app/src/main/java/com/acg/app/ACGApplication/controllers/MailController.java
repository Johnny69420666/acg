package com.acg.app.ACGApplication.controllers;

import com.acg.app.ACGApplication.commands.MailCommand;
import com.acg.app.ACGApplication.services.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;

@RestController
@RequestMapping("/api/email")
public class MailController {

    private final MailService mailService;

    @Autowired
    public MailController(MailService mailService) {
        this.mailService = mailService;
    }

    @PostMapping(path = "/contact")
    public void sendContactMail(@RequestBody MailCommand command) throws MessagingException {
        mailService.sendContactMail(command.getName(),command.getSurname(),command.getPhone(),command.getEmail(), command.getSubject(), command.getText());
    }
}