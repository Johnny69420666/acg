package com.acg.app.ACGApplication.services;

import com.acg.app.ACGApplication.models.Image;

import java.util.List;

public interface ImageService {

    Image findImageById(String uuid);

    List<Image> getCarImagesByCarId(String uuid);

    void deleteImageById(String uuid);
}
