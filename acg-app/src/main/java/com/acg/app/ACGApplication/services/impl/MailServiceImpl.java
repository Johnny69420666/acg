package com.acg.app.ACGApplication.services.impl;

import com.acg.app.ACGApplication.services.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class MailServiceImpl implements MailService {

    private final JavaMailSender javaMailSender;

    @Autowired
    public MailServiceImpl(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void sendContactMail(String name, String surname, String phone,String email, String subject, String text) throws MessagingException {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        String[] to = {"autocentargrubisic@gmail.com", email};

        helper.setTo(to);
        helper.setSubject(subject);
        helper.setText(text + "\n\n" + name + " " + surname + " " + phone);

        javaMailSender.send(message);
    }

    @Override
    public void sendAdminMail(String username, String password) throws MessagingException {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setTo("autocentargrubisic@gmail.com");
        helper.setSubject("Registracija");
        helper.setText("username: "+username +"\npassword: "+password);

        javaMailSender.send(message);
    }
}
