package com.acg.app.ACGApplication.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AuthenticateCommand {

    private String username;
    private String password;
}
