package com.acg.app.ACGApplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AcgAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(AcgAppApplication.class, args);
	}

}
