package com.acg.app.ACGApplication.services.impl;

import com.acg.app.ACGApplication.commands.CarCommand;
import com.acg.app.ACGApplication.commands.PagingCommand;
import com.acg.app.ACGApplication.commands.ScrapedData;
import com.acg.app.ACGApplication.commands.SortingInfoDto;
import com.acg.app.ACGApplication.components.HttpsClient;
import com.acg.app.ACGApplication.converters.CarCommandToCar;
import com.acg.app.ACGApplication.models.Car;
import com.acg.app.ACGApplication.models.Image;
import com.acg.app.ACGApplication.models.Info;
import com.acg.app.ACGApplication.repositories.CarRepository;
import com.acg.app.ACGApplication.repositories.ImageRepository;
import com.acg.app.ACGApplication.repositories.InfoRepository;
import com.acg.app.ACGApplication.services.CarService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.data.domain.Sort.Direction.ASC;
import static org.springframework.data.domain.Sort.Direction.DESC;

@Service
public class CarServiceImpl implements CarService {

    private final HttpsClient httpsClient;
    private final CarRepository carRepository;
    private final ImageRepository imageRepository;
    private final CarCommandToCar carCommandToCar;
    private final InfoRepository infoRepository;
    private static final Logger logger = LogManager.getLogger(CarService.class);


    @Autowired
    public CarServiceImpl(HttpsClient httpsClient, CarRepository carRepository, ImageRepository imageRepository, CarCommandToCar carCommandToCar, InfoRepository infoRepository) {
        this.httpsClient = httpsClient;
        this.carRepository = carRepository;
        this.imageRepository = imageRepository;
        this.carCommandToCar = carCommandToCar;
        this.infoRepository = infoRepository;
    }

    @Override
    public List<Car> findExpensive() {
        return carRepository.findExpensiveCars();
    }

    @Override
    public List<Car> findAllCars(){
        return carRepository.findAll();
    }

    @Override
    public Car findCarById(UUID uuid){
        if (carRepository.findById(uuid).isPresent()){
            return carRepository.findById(uuid).get();
        } else {
            logger.info("Car with id " + uuid + " not found");
            return null;
        }
    }

    @Override
    public Optional<Car> findCarByUrl(String url){
        return carRepository.findByUrl(url);
    }

    @Override
    public Page pageCars(PagingCommand command){

        Sort sort = Sort.by(DESC,"created");

        if (command.getPrice()!=null && command.getPrice().equals("asc")){
            sort = Sort.by(ASC,"price");
        } else if (command.getPrice()!=null && command.getPrice().equals("desc")){
            sort = Sort.by(DESC,"price");
        }

        if (command.getMake() != null && command.getModel() == null && command.getType() == null) {
            return carRepository.findAllByParams(
                    command.getProductionYearFrom(),
                    command.getProductionYearTo(),
                    command.getMileageFrom(),
                    command.getMileageTo(),
                    command.getMake(),
                    PageRequest.of(command.getPage(), 9, sort));
        } else if (command.getMake() != null && command.getModel() != null && command.getType() == null) {
            return carRepository.findAllByParams(
                    command.getProductionYearFrom(),
                    command.getProductionYearTo(),
                    command.getMileageFrom(),
                    command.getMileageTo(),
                    command.getMake(),
                    command.getModel(),
                    PageRequest.of(command.getPage(), 9, sort));
        } else if (command.getMake() != null && command.getModel() != null && command.getType() != null) {
            return carRepository.findAllByParams(
                    command.getProductionYearFrom(),
                    command.getProductionYearTo(),
                    command.getMileageFrom(),
                    command.getMileageTo(),
                    command.getMake(),
                    command.getModel(),
                    command.getType(),
                    PageRequest.of(command.getPage(), 9, sort));
        }
        return carRepository.findAllByParams(
                command.getProductionYearFrom(),
                command.getProductionYearTo(),
                command.getMileageFrom(),
                command.getMileageTo(),
                PageRequest.of(command.getPage(), 9, sort));

    }

    @Override
    public Page<Car> getFeaturedCars(){
        return carRepository.findAlFeatured(PageRequest.of(0, 3, Sort.by("created")));
    }

    @Override
    @Transactional
    public void saveImageFileToImageList(String uuid, MultipartFile file){
        Car car = findCarById(UUID.fromString(uuid));
        List<Image> images = car.getImages();

        Byte[] byteObjects = new Byte[0];
        int i=0;

        try {
            byteObjects = new Byte[file.getBytes().length];
            for (byte b : file.getBytes()){
                byteObjects[i++] = b;
            }
            Image image = new Image();
            image.setData(byteObjects);
            image.setCar(car);
            images.add(image);
            imageRepository.save(image);
            if (car.getProfilePicture() == null){car.setProfilePicture(image.getId().toString());}

        } catch (IOException e) {
            e.printStackTrace();
        }
        carRepository.save(car);
    }

    @Override
    @Transactional
    public void editCarById(String uuid, CarCommand source){
        Car car = findCarById(UUID.fromString(uuid));
        Info info = car.getInfo();

        car.setType(source.getType());
        car.setPrice(source.getPrice());
        car.setModel(source.getModel());
        car.setMake(source.getMake());
        car.setMileage(source.getMileage());
        car.setProductionYear(source.getProductionYear());
        info.setLocation(source.getLocation());
        info.setGearbox(source.getGearbox());
        info.setMotor(source.getMotor());
        info.setModelYear(source.getModelYear());
        info.setMotorPower(source.getMotorPower());
        info.setMotorCubicCentimeters(source.getMotorCubicCentimeters());
        info.setNumberOfGears(source.getNumberOfGears());
        info.setGaraged(source.getGaraged());
        info.setHasServiceBook(source.getHasServiceBook());
        info.setUsed(source.getUsed());
        car.setInfo(info);

        infoRepository.save(info);
        carRepository.save(car);
    }

    @Override
    @Transactional
    public String saveNewCar(MultipartFile[] images, String carCommandJson) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        CarCommand carCommand = mapper.readValue(carCommandJson, CarCommand.class);

        Car car = carCommandToCar.convert(carCommand);
        carRepository.save(car);
        //todo compression / thumbnails?
        for(MultipartFile image : images){
            saveImageFileToImageList(car.getId().toString(),image);
        }
        carRepository.save(car);
        return car.getId().toString();
    }

    @Override
    @Transactional
    public String addCarViaUrl(String https_url) throws IOException, InterruptedException {
        if (carRepository.findByUrl(https_url).isPresent()) {
            logger.info("Car with url " + https_url + " already exists!");
            return "Car already exists";
        }

        ScrapedData data = httpsClient.scrapeDataFromCarPage(https_url);
        Car car = carCommandToCar.convert(data.getCar());
        assert car != null;
        carRepository.save(car);
        logger.info("Image count:" + data.getImages().size());


        int counter = 1;
        for (String link : data.getImages()){
            logger.info("Uploading image "+ counter);
            URL url = new URL(link);
            logger.info("image url:"+ url);
            BufferedImage image = ImageIO.read(url);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ImageIO.write(image,"jpg",byteArrayOutputStream);
            byteArrayOutputStream.flush();

            Image imageToSave = new Image();


            Byte[] byteObjects;
            int i=0;
            byteObjects = new Byte[byteArrayOutputStream.toByteArray().length];
            for (byte b : byteArrayOutputStream.toByteArray()){
                byteObjects[i++] = b;
            }
            imageToSave.setData(byteObjects);
            imageToSave.setCar(car);
            imageRepository.save(imageToSave);
            if(car.getProfilePicture() == null){
                car.setProfilePicture(imageToSave.getId().toString());
            }
            byteArrayOutputStream.close();
            counter++;
        }

        carRepository.save(car);
        logger.info("Added car!");
        return car.getId().toString();
    }

    @Override
    @Transactional
    public void deleteCarById(String uuid){
        Car car =  findCarById(UUID.fromString(uuid));
        carRepository.delete(car);
    }

    @Override
    public void sellCarById(String uuid){
        Car car = findCarById(UUID.fromString(uuid));
        car.setSold(true);
        carRepository.save(car);
    }

    @Override
    public void unsellCarById(String uuid) {
        Car car = findCarById(UUID.fromString(uuid));
        car.setSold(false);
        carRepository.save(car);
    }

    @Override
    public void changeProfilePicture(String carId, String imageId){
        Car car = findCarById(UUID.fromString(carId));
        car.setProfilePicture(imageId);
        carRepository.save(car);
    }

    @Override
    public  List<String> findAllMakes(){
        return carRepository.findDistinctCarMakes();
    }

    @Override
    public List<String> findAllMakeModels(String make){
        return carRepository.findDistinctModelsOfMake(make);
    }

    @Override
    public List<String> findAllTypesByMakeAndModel(String make, String model){
        return carRepository.findDistinctTypesByMakeAndModel(make,model);
    }

    @Override
    public Integer updateCars() throws IOException, InterruptedException {
        logger.info("Updating cars.");
        int numOfCars = httpsClient.getNumOfCars();
        logger.info("Found " + numOfCars + " cars.");
        int addedCars = 0;
        int i = 0;
        for (i=0;i<=Math.ceil((double)numOfCars/(double)20);i++){
            List<String> links = httpsClient.scrapeCarLinksFromUrl("https://www.njuskalo.hr/trgovina/autocentar-grubisic?page="+i);
            for (String link : links){
                Optional<Car> car = findCarByUrl(link);
                if(car.isEmpty()){
                    logger.info("Adding new car, url: " + link);
                    addCarViaUrl(link);
                    addedCars++;
                }
            }
        }
        logger.info("Updating cars finished. Found " + numOfCars + " cars, added " + addedCars + " new cars");

        return addedCars;
    }

    @Override
    public SortingInfoDto getInitialSortingData(){
        SortingInfoDto sortingInfoDto = new SortingInfoDto();

        sortingInfoDto.setMaxMileage(carRepository.getMaxMileage());
        sortingInfoDto.setMinMileage(carRepository.getMinMileage());
        sortingInfoDto.setMaxProductionYear(carRepository.getMaxProductionYear());
        sortingInfoDto.setMinProductionYear(carRepository.getMinProductionYear());

        return sortingInfoDto;
    }

    @Override
    public void featureCarById(String uuid){
        Car car = findCarById(UUID.fromString(uuid));
        car.setFeatured(true);
        carRepository.save(car);
    }

    @Override
    public void unFeatureCarById(String uuid){
        Car car = findCarById(UUID.fromString(uuid));
        car.setFeatured(false);
        carRepository.save(car);
    }
}