package com.acg.app.ACGApplication.repositories;

import com.acg.app.ACGApplication.models.Admin;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.UUID;

public interface AdminRepository extends CrudRepository<Admin, UUID> {

    @Query(value = "select * from admin where username =:username",nativeQuery = true)
    Optional<Admin> findByUsername(@Param("username") String username);

}
