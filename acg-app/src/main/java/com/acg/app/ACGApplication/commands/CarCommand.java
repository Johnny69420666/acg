package com.acg.app.ACGApplication.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CarCommand {
    private String make,model,type,location,gearbox,motor,description,njuskalo,owner;
    private Long mileage,price;
    private Integer productionYear,modelYear,motorPower,motorCubicCentimeters,numberOfGears;
    private Boolean hasServiceBook,garaged,used;
}
