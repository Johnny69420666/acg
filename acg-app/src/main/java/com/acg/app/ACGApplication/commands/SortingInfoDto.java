package com.acg.app.ACGApplication.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SortingInfoDto {
    private Long minMileage, maxMileage;
    private Integer minProductionYear, maxProductionYear;
}
