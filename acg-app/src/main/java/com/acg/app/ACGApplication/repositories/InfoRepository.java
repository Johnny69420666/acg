package com.acg.app.ACGApplication.repositories;

import com.acg.app.ACGApplication.models.Info;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface InfoRepository extends CrudRepository<Info, UUID> {
}
