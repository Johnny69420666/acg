package com.acg.app.ACGApplication.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "info")
public class Info {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", length = 16, columnDefinition = "BINARY(16)")
    private UUID uuid;
    private String location;
    private String gearbox;
    private String motor;
    private String owner;
    @Column(name = "model_year")
    private Integer modelYear;
    @Column(name = "motor_power")
    private Integer motorPower;
    @Column(name = "motor_cubic_centimeters")
    private Integer motorCubicCentimeters;
    @Column(name = "number_of_gears")
    private Integer numberOfGears;
    @Column(name = "is_used")
    private Boolean isUsed;
    @Column(name = "has_service_book")
    private Boolean hasServiceBook;
    @Column(name = "is_garaged")
    private Boolean isGaraged;
    @Lob
    private String description;

    @OneToOne(mappedBy = "info")
    @JsonIgnore
    private Car car;

    public Info() { }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getModelYear() {
        return modelYear;
    }

    public void setModelYear(Integer modelYear) {
        this.modelYear = modelYear;
    }

    public String getMotor() {
        return motor;
    }

    public void setMotor(String motor) {
        this.motor = motor;
    }

    public Integer getMotorPower() {
        return motorPower;
    }

    public void setMotorPower(Integer motorPower) {
        this.motorPower = motorPower;
    }

    public Integer getMotorCubicCentimeters() {
        return motorCubicCentimeters;
    }

    public void setMotorCubicCentimeters(Integer motorCubicCentimeters) {
        this.motorCubicCentimeters = motorCubicCentimeters;
    }

    public String getGearbox() {
        return gearbox;
    }

    public void setGearbox(String gearbox) {
        this.gearbox = gearbox;
    }

    public Integer getNumberOfGears() {
        return numberOfGears;
    }

    public void setNumberOfGears(Integer numberOfGears) {
        this.numberOfGears = numberOfGears;
    }

    public Boolean getUsed() {
        return isUsed;
    }

    public void setUsed(Boolean used) {
        isUsed = used;
    }

    public Boolean getHasServiceBook() {
        return hasServiceBook;
    }

    public void setHasServiceBook(Boolean hasServiceBook) {
        this.hasServiceBook = hasServiceBook;
    }

    public Boolean getGaraged() {
        return isGaraged;
    }

    public void setGaraged(Boolean garaged) {
        isGaraged = garaged;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Info)) return false;
        Info info = (Info) o;
        return getUuid().equals(info.getUuid());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUuid());
    }

    @Override
    public String toString() {
        return "Info{" +
                "uuid=" + uuid +
                ", location='" + location + '\'' +
                ", gearbox='" + gearbox + '\'' +
                ", motor='" + motor + '\'' +
                ", modelYear=" + modelYear +
                ", motorPower=" + motorPower +
                ", motorCubicCentimeters=" + motorCubicCentimeters +
                ", numberOfGears=" + numberOfGears +
                ", isUsed=" + isUsed +
                ", hasServiceBook=" + hasServiceBook +
                ", isGaraged=" + isGaraged +
                ", description='" + description + '\'' +
                ", car=" + car +
                '}';
    }
}
