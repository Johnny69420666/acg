package com.acg.app.ACGApplication.converters;

import com.acg.app.ACGApplication.commands.CarCommand;
import com.acg.app.ACGApplication.models.Car;
import com.acg.app.ACGApplication.models.Info;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class CarCommandToCar implements Converter<CarCommand, Car> {

    @Synchronized
    @Nullable
    @Override
    public Car convert(CarCommand source){
        if (source == null){
            return null;
        }

        Car car = new Car();
        Info info = new Info();
        car.setType(source.getType());
        car.setPrice(source.getPrice());
        car.setModel(source.getModel());
        car.setMake(source.getMake());
        car.setNjuskalo(source.getNjuskalo());
        car.setMileage(source.getMileage());
        car.setProductionYear(source.getProductionYear());
        info.setOwner(source.getOwner());
        info.setLocation(source.getLocation());
        info.setGearbox(source.getGearbox());
        info.setMotor(source.getMotor());
        info.setModelYear(source.getModelYear());
        info.setMotorPower(source.getMotorPower());
        info.setMotorCubicCentimeters(source.getMotorCubicCentimeters());
        info.setNumberOfGears(source.getNumberOfGears());
        info.setGaraged(source.getGaraged());
        info.setHasServiceBook(source.getHasServiceBook());
        info.setUsed(source.getUsed());
        info.setDescription(source.getDescription());
        car.setInfo(info);

        return car;
    }
}
