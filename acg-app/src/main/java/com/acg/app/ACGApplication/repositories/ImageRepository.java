package com.acg.app.ACGApplication.repositories;

import com.acg.app.ACGApplication.models.Image;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface ImageRepository extends CrudRepository<Image, UUID> {
}
