package com.acg.app.ACGApplication.components;

import com.acg.app.ACGApplication.models.Admin;
import com.acg.app.ACGApplication.models.Car;
import com.acg.app.ACGApplication.models.Image;
import com.acg.app.ACGApplication.models.Info;
import com.acg.app.ACGApplication.repositories.AdminRepository;
import com.acg.app.ACGApplication.repositories.CarRepository;
import com.acg.app.ACGApplication.services.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import javax.mail.MessagingException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

@Component
public class PrepopulateDatabase implements ApplicationListener<ContextRefreshedEvent> {

    private final AdminRepository adminRepository;
    private final MailService mailService;
    private final PasswordEncoder passwordEncoder;
    private final CarRepository carRepository;


    private static final String CHAR_LOWER = "abcdefghijklmnopqrstuvwxyz";
    private static final String CHAR_UPPER = CHAR_LOWER.toUpperCase();
    private static final String NUMBER = "0123456789";

    private static final String DATA_FOR_RANDOM_STRING = CHAR_LOWER + CHAR_UPPER + NUMBER;
    private static SecureRandom random = new SecureRandom();

    @Autowired
    public PrepopulateDatabase(AdminRepository adminRepository, MailService mailService, PasswordEncoder passwordEncoder, CarRepository carRepository) {
        this.adminRepository = adminRepository;
        this.mailService = mailService;
        this.passwordEncoder = passwordEncoder;
        this.carRepository = carRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        List<Admin> admins = new ArrayList<>();
        for (Admin admin: adminRepository.findAll()){
            admins.add(admin);
        }
        if(admins.size() == 0){
            try {
                generateAdmin();
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }
    }

    private void generateAdmin() throws MessagingException {
        adminRepository.deleteAll();
        Admin admin = new Admin();
        String password = generateRandomString();
        admin.setPassword(passwordEncoder.encode(password));
        mailService.sendAdminMail("admin", password);
        adminRepository.save(admin);
    }

    private static String generateRandomString() {

        StringBuilder sb = new StringBuilder(10);
        for (int i = 0; i < 10; i++) {

            // 0-62 (exclusive), random returns 0-61
            int rndCharAt = random.nextInt(DATA_FOR_RANDOM_STRING.length());
            char rndChar = DATA_FOR_RANDOM_STRING.charAt(rndCharAt);

            sb.append(rndChar);
        }

        return sb.toString();

    }
}