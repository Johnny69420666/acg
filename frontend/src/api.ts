import axios from "axios";
import {Car} from "./types/Car";
import {PagingDto} from "./types/PagingDto";
import {PagingCommand} from "./types/PagingCommand";
import {CarPreviewDto} from "./types/CarPreviewDto";


export async function getCarByCarId(id:string):Promise<Car> {
    let car:Car
    const resp1 = await axios.get(process.env.REACT_APP_API_URL+'/cars/'+id)
    const resp2 = await axios.get(process.env.REACT_APP_API_URL+'/info/'+id)
    car = {...resp1.data,...resp2.data}
    return car
}

export async function getPagedCarsByPageNumber(pagingCommand:PagingCommand):Promise<PagingDto> {
    const resp = await axios.post(process.env.REACT_APP_API_URL+"/cars/page", pagingCommand)
    return resp.data
}

export async function handleAuth(val: {username: string, password: string}) {
    const resp = await axios.post(process.env.REACT_APP_API_URL+"/admin/auth", val)
    return resp.data
}

export async function sendEmail(val:{name:string,surname:string,phone:string,email:string, subject:string, text:string}) {
    const resp = await axios.post(process.env.REACT_APP_API_URL+"/email/contact", val)
    return resp
}

export async function editCar(val:Car) {
    const resp = await axios.post(process.env.REACT_APP_API_URL+'/cars/' + val.id, val)
    return resp
}

export async function postCar(formData:FormData) {
    const resp = await axios.post(process.env.REACT_APP_API_URL+'/cars/new', formData)
    return resp
}

export async function   getCarImageIdListByCarId(uuid:string) {
    const resp = await axios.get(process.env.REACT_APP_API_URL+'/images/car/' + uuid)
    return resp
}

export async function addNewCarFromUrl(https_url:string) {
    const resp = await axios.post(process.env.REACT_APP_API_URL+'/cars/new/url', https_url)
    return resp.data;
}

export async function deleteCarById(uuid:string) {
    const resp = await axios.post(process.env.REACT_APP_API_URL+'/cars/delete', uuid)
    return resp;
}

export async function sellCarById(uuid:string) {
    const resp = await axios.post(process.env.REACT_APP_API_URL+'/cars/sell', uuid)
    return resp
}

export async function unsellCarById(uuid:string) {
    const resp = await axios.post(process.env.REACT_APP_API_URL+"/cars/unsell", uuid)
    return resp
}

export async function featureCarById(uuid:string) {
    const resp = await axios.post(process.env.REACT_APP_API_URL+"/cars/feature", uuid)
    return resp
}

export async function unFeatureCarById(uuid:string) {
    const resp = await axios.post(process.env.REACT_APP_API_URL+"/cars/un-feature", uuid)
    return resp
}

    export async function getFeaturedCars():Promise<CarPreviewDto[]> {
    const resp = await axios.get(process.env.REACT_APP_API_URL+"/cars/expensive")
    return resp.data
}

export async function deleteImageById(uuid:string) {
    const resp = await axios.post(process.env.REACT_APP_API_URL+'/images/delete', uuid)
    return resp
}

export async function changeProfilePicture(carId:string, imageId:string) {
    const resp = await axios.post(process.env.REACT_APP_API_URL+'/cars/' + carId + '/profile', imageId)
    return resp
}

export async function refreshAllCars():Promise<number> {
    const resp = await axios.post(process.env.REACT_APP_API_URL+'/cars/refresh-all')
    return resp.data
}

export async function getAllMakes():Promise<string[]> {
    const resp = await axios.get(process.env.REACT_APP_API_URL+'/cars/makes')
    return resp.data
}

export async function getAllMakeModels(make:string):Promise<string[]> {
    const resp = await axios.get(process.env.REACT_APP_API_URL+'/cars/'+make+'/models')
    return resp.data
}

export async function getAllTypesByMakeAndModel(make:string,model:string):Promise<string[]> {
    const resp = await axios.get(process.env.REACT_APP_API_URL+'/cars/' + make + '/' + model + '/types')
    return resp.data
}

export async function getInitialSortingData():Promise<{ minMileage:number, maxMileage:number, minProductionYear:number, maxProductionYear:number }> {
    const resp = await axios.get(process.env.REACT_APP_API_URL+"/cars/initial-sorting-data")
    return resp.data
}
