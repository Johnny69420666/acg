import {createContext} from "react";

const AdminCtx = createContext<{ authenticated: boolean, updateAuthenticated: (arg0: boolean) => void}>({authenticated: false, updateAuthenticated: () => {}})

export default AdminCtx