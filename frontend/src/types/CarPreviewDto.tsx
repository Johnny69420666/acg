export type CarPreviewDto = {
    id: string,
    price: number,
    make: string,
    model: string,
    type: string,
    sold:boolean,
    profilePicture:string,
    mileage: number,
    productionYear: number,
    featured:boolean,
}