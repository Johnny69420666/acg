export type ImageItem = {
    original:string,
    thumbnail:string
}