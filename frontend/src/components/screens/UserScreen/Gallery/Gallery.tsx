import React from 'react'
import ImageGallery from 'react-image-gallery';
import img1 from "../../../../images/GALERIJA/img1.jpg"
import img2 from "../../../../images/GALERIJA/img2.jpg"
import img3 from "../../../../images/GALERIJA/img3.jpg"
import img4 from "../../../../images/GALERIJA/img4.jpg"
import img5 from "../../../../images/GALERIJA/img5.jpg"
import img6 from "../../../../images/GALERIJA/img6.jpg"
import img7 from "../../../../images/GALERIJA/img7.jpg"
import img8 from "../../../../images/GALERIJA/img8.jpg"
import img9 from "../../../../images/GALERIJA/img9.jpg"
import img10 from "../../../../images/GALERIJA/img10.jpg"
import img11 from "../../../../images/GALERIJA/img11.jpg"
import img12 from "../../../../images/GALERIJA/img12.jpg"
import img13 from "../../../../images/GALERIJA/img13.jpg"
import img14 from "../../../../images/GALERIJA/img14.jpg"
import img15 from "../../../../images/GALERIJA/img15.jpg"
import img16 from "../../../../images/GALERIJA/img16.jpeg"
import img17 from "../../../../images/GALERIJA/img17.jpg"
import img18 from "../../../../images/GALERIJA/img18.jpg"
import img19 from "../../../../images/GALERIJA/img19.jpg"
import img20 from "../../../../images/GALERIJA/img20.jpg"
import img21 from "../../../../images/GALERIJA/img21.jpg"
import img22 from "../../../../images/GALERIJA/img22.jpg"
import img23 from "../../../../images/GALERIJA/img23.jpg"
import img24 from "../../../../images/GALERIJA/img24.jpg"
import img25 from "../../../../images/GALERIJA/img25.jpg"
import img26 from "../../../../images/GALERIJA/img26.jpg"
import img27 from "../../../../images/GALERIJA/img27.jpg"
import img28 from "../../../../images/GALERIJA/img28.jpg"

const images = [
    {
        original:img1,
        thumbnail:img1,
    },
    {
        original:img2,
        thumbnail:img2,
    },
    {
        original:img3,
        thumbnail:img3,
    },
    {
        original:img4,
        thumbnail:img4,
    },
    {
        original:img5,
        thumbnail:img5,
    },
    {
        original:img6,
        thumbnail:img6,
    },
    {
        original:img7,
        thumbnail:img7,
    },
    {
        original:img8,
        thumbnail:img8,
    },
    {
        original:img9,
        thumbnail:img9,
    },
    {
        original:img10,
        thumbnail:img10,
    },
    {
        original:img11,
        thumbnail:img11,
    },
    {
        original:img12,
        thumbnail:img12,
    },
    {
        original:img13,
        thumbnail:img13,
    },
    {
        original:img14,
        thumbnail:img14,
    },
    {
        original:img15,
        thumbnail:img15,
    },
    {
        original:img16,
        thumbnail:img16,
    },
    {
        original:img17,
        thumbnail:img17,
    },
    {
        original:img18,
        thumbnail:img18,
    },
    {
        original:img19,
        thumbnail:img19,
    },
    {
        original:img20,
        thumbnail:img20,
    },
    {
        original:img21,
        thumbnail:img21,
    },
    {
        original:img22,
        thumbnail:img22,
    },
    {
        original:img23,
        thumbnail:img22,
    },
    {
        original:img24,
        thumbnail:img24,
    },
    {
        original:img25,
        thumbnail:img25,
    },
    {
        original:img26,
        thumbnail:img26,
    },
    {
        original:img27,
        thumbnail:img27,
    },
    {
        original:img28,
        thumbnail:img28,
    },
]

const Gallery:React.FC = () => {
    return(
        <>
            <div className={"LMAOXD"}>
            <ImageGallery
                thumbnailPosition={"bottom"}
                items={images}
                showFullscreenButton={false}
                autoPlay={true}
                additionalClass={"sisicaaaa"}
            />
            </div>
        </>
    );
}

export default Gallery