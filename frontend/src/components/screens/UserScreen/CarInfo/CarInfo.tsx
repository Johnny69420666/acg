import React, {useContext, useEffect, useState} from "react";
import {Button, Table} from "reactstrap";
import {getCarByCarId, getCarImageIdListByCarId} from "../../../../api";
import {Car} from "../../../../types/Car";
import {ImageItem} from "../../../../types/ImageItem";
import ImageGallery from 'react-image-gallery';
import AdminCtx from "../../../../contexts/AdminCtx";
import {navigate} from "hookrouter";
import {IoLogoEuro} from "react-icons/io";
import CurrencyContext from "../../../../contexts/CurrencyContext";

type Props = {
    uuid:string
}

const CarInfo:React.FC<Props> = (props) => {

    const [car, setCar] = useState<Car>()
    const [images, setImages] = useState<ImageItem[] | null>(null)

    const adminCtx = useContext(AdminCtx)
    const currencyCtx = useContext(CurrencyContext)

    useEffect(() => {
        getCarByCarId(props.uuid).then((response) => (setCar(response),console.log(response.hasServiceBook)))
        getCarImageIdListByCarId(props.uuid).then((response) => {
            let i;
            let array = [];
            for(i=0;i<response.data.length;i++){
                array.push({
                    original:process.env.REACT_APP_API_URL+"/images/"+response.data[i].id,
                    thumbnail:process.env.REACT_APP_API_URL+"/images/"+response.data[i].id
                })
            }
            console.log("data->"+response.data)
            console.log("images->"+array)
            setImages(array)
        })
    },[props.uuid])

    return(
        <div className="carProfileWrapper">
            <div className="pics">
                {images && <ImageGallery
                    thumbnailPosition={"bottom"}
                    items={images}
                    showFullscreenButton={true}
                    autoPlay={true}
                    additionalClass={"sisicaaaa"}
                />}
            </div>
            <div className="info">
                {adminCtx.authenticated && <Button color="info" onClick={() => {navigate('/admin/edit/' + props.uuid)}}>Edit</Button>}
                {car?
                    (<Table>
                        <tbody>
                            <tr>
                                <td className="carInfoLabel">Cijena:</td>
                                <td style={{textAlign:"left"}}>
                                    {currencyCtx.currency==="EUR"? (<>{car.price}<IoLogoEuro/></>) : null}
                                    {currencyCtx.currency==="HRK"? (<>~{Math.round(car.price * 7.44)}<b>kn</b></>) : null}
                                </td>
                            </tr>
                            <tr>
                                <td className="carInfoLabel">Lokacija vozila:</td>
                                <td style={{textAlign:"left"}}>{car.location}</td>
                            </tr>
                            <tr>
                                <td className="carInfoLabel">Marka automobila:</td>
                                <td style={{textAlign:"left"}}>{car.make}</td>
                            </tr>
                            <tr>
                                <td className="carInfoLabel">Model automobila:</td>
                                <td style={{textAlign:"left"}}>{car.model}</td>
                            </tr>
                            <tr>
                                <td className="carInfoLabel">Tip automobila:</td>
                                <td style={{textAlign:"left"}}>{car.type}</td>
                            </tr>
                            <tr>
                                <td className="carInfoLabel">Godina proizvodnje:</td>
                                <td style={{textAlign:"left"}}>{car.productionYear} godište</td>
                            </tr>
                            <tr>
                                <td className="carInfoLabel">Godina modela:</td>
                                <td style={{textAlign:"left"}}>{car.modelYear}</td>
                            </tr>
                            <tr>
                                <td className="carInfoLabel">Prijeđeni kilometri:</td>
                                <td style={{textAlign:"left"}}>{car.mileage} km</td>
                            </tr>
                            <tr>
                                <td className="carInfoLabel">Motor:</td>
                                <td style={{textAlign:"left"}}>{car.motor}</td>
                            </tr>
                            <tr>
                                <td className="carInfoLabel">Snaga motora:</td>
                                <td style={{textAlign:"left"}}>{car.motorPower}  kW</td>
                            </tr>
                            <tr>
                                <td className="carInfoLabel">Radni obujam:</td>
                                <td style={{textAlign:"left"}}>{car.motorCubicCentimeters} cm³</td>
                            </tr>
                            <tr>
                                <td className="carInfoLabel">Mjenjač:</td>
                                <td style={{textAlign:"left"}}>{car.gearbox}</td>
                            </tr>
                            {car.numberOfGears && <tr>
                                <td className="carInfoLabel">Broj stupnjeva:</td>
                                <td style={{textAlign:"left"}}>{car.numberOfGears}</td>
                            </tr>}
                            <tr>
                                <td className="carInfoLabel">Stanje:</td>
                                <td style={{textAlign:"left"}}>{car.used? ("rabljeno"):("nov")}</td>
                            </tr>
                            <tr>
                                <td className="carInfoLabel">Servisna knjiga:</td>
                                <td style={{textAlign:"left"}}>{car.hasServiceBook? ("Da"):("Ne")}</td>
                            </tr>
                            <tr>
                                <td className="carInfoLabel">Garažiran:</td>
                                <td style={{textAlign:"left"}}>{car.garaged? ("Da"):("Ne")}</td>
                            </tr>
                        </tbody>
                    </Table>
                    ) : (
                        <>Loading...</>
                    )
                }
            </div>
            <div className="desc">
                {car && <p style={{whiteSpace:"pre-wrap",textAlign:"left"}}>{car.description}</p>}
            </div>
        </div>
    );
}

export default CarInfo