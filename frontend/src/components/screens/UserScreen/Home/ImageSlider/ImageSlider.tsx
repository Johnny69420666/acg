import React, {useEffect, useState} from 'react'
import {Carousel, CarouselCaption, CarouselIndicators, CarouselItem} from "reactstrap";
import acg from '../images/acg.jpg'
import plac from '../images/plac.jpg'

const ImageSlider:React.FC = () => {

    const items = [
        {
            src: acg,
            altText: 'Slide 1',
            caption: 'Slide 1'
        },
        {
            src: plac,
            altText: 'Slide 2',
            caption: 'Slide 2'
        },
    ];

    const [activeIndex, setActiveIndex] = useState(0);
    const [animating, setAnimating] = useState(false);

    const next = () => {
        if (animating) return;
        const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
        setActiveIndex(nextIndex);
    }

    const previous = () => {
        if (animating) return;
        const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
        setActiveIndex(nextIndex);
    }

    const goToIndex = (newIndex:number) => {
        if (animating) return;
        setActiveIndex(newIndex);
    }

    useEffect(()=>{
        // getPagedCarsByPageNumber(({page:0,price:'',model:'',make:'',type:''})).then(response=>{
        //     let helper = []
        //     let i = 0;
        //     for(i;i<response.content.length;i++){
        //         helper.push({
        //             src:"http://localhost:8080/acg/api/images/"+response.content[i].profilePicture,
        //             altText:"auto",
        //             caption:"car"
        //         })
        //     }
        //     setItems(helper)
        // })
    },[])

    const slides = items.map((item) => {
        return (
            <CarouselItem
                onExiting={() => setAnimating(true)}
                onExited={() => setAnimating(false)}
                key={item.src}
            >
                <img src={item.src} alt={item.altText} style={{objectFit:"contain"}}/>
                {!item.src && <CarouselCaption captionText={item.caption} captionHeader={item.caption} />}
            </CarouselItem>
        );
    });

    return(
        <Carousel
            activeIndex={activeIndex}
            next={next}
            previous={previous}
            indicators={true}
        >
            <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={goToIndex} />
            {slides}
        </Carousel>
    );
}

export default ImageSlider