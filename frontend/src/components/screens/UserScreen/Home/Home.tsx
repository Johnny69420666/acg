import React, {useEffect, useState} from 'react'
import {navigate} from "hookrouter";
import {CarPreviewDto} from "../../../../types/CarPreviewDto";
import {getFeaturedCars} from "../../../../api";
import Car from "../Cars/Car/Car";
import prva from "../../../../images/frontpage/prva.jpeg"
import druga from "../../../../images/frontpage/druga.jpeg"
import treca from "../../../../images/frontpage/treca.jpg"
import ImageGallery from "react-image-gallery";
//nek nas prati sreca

const items = [
    {
        original:prva,
        thumbnail:prva,
    },
    {
        original:druga,
        thumbnail:druga,
    },
    {
        original:treca,
        thumbnail:treca,
    },
];

const Home:React.FC = () => {

    const [featuredCars, setFeaturedCars] = useState<CarPreviewDto[]>([])

    useEffect(()=>{
        getFeaturedCars().then(response => {
            setFeaturedCars(response)
        })
    },[]);

    return(
        <div className="homeGrid">
            <div className={"vrcenjeSlika"}>
                <ImageGallery
                    thumbnailPosition={"bottom"}
                    items={items}
                    showFullscreenButton={false}
                    showPlayButton={false}
                    showNav={false}
                    autoPlay={false}
                    additionalClass={"sisicaaaa"}
                />
            </div>
            {featuredCars.length>0 &&
            <>
                <h4>Izdvojeno iz ponude:</h4>
                <div className="featuredCars">
                    {featuredCars.map(car=>
                        <>
                            <Car
                                car={car}
                                func={()=>{navigate('/auti/'+car.id)}}
                            />
                        </>
                    )}
                </div>
            </>
            }
        </div>
    );
}

export default Home