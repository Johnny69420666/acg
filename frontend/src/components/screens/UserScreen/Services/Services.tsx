import React from 'react'
import handcarkey from "../../../../images/hand-car-key.png";
import adriaticimage from "../../../../images/adriatic-image.png"
import laptop from "../../../../images/laptop.png"
import golfovi from "../../../../images/golfovi.png"

const Services:React.FC = () => {
    return(
        <div className="ServicesWrapper">
            <div className="import">
                <h3>Uvoz vozila po narudžbi</h3>
                <p>Ukoliko u našoj ponudi ne možete pronaći vozilo po Vašem izboru obratite nam se sa povjerenjem te ćemo pronaći vozilo za Vas na njemačkom tržištu strogo u ovlaštenoj auto kući te odraditi svu potrebnu papirologiju za Vas. Za sve upite, savjete i informacije molim Vas da popunite obrazac u rubrici “kontakt” kako bismo Vas mogli kontaktirati i pomoći u što kraćem roku.</p>
                <div  style={{padding:"0px"}}><img src={handcarkey} style={{
                    "maxWidth": "125%",
                    "maxHeight": "125%",
                }}/></div>
            </div>
            <div className="garauntee">
                <h3>Obvezno auto osiguranje</h3>
                <p>Obveznim osiguranjem od automobilske odgovonosti osiguravate svoju odgovornost prema trećim osobama za štete nastale vašim vozilom u zemlji i inozemstvu. Također, možete ugovoriti i uslugu pomoći na cesti ukoliko Vaše vozilo ostane u kvaru i ostale brojne pogodnosti. Za sve informacije i izračun premije police auto odgovornosti svakako nas kontaktirajte na broj 099 2320 300, a mi ćemo svakom upitu pristupiti individualno i tako opravdati vaše povjerenje.</p>
                <div><img src={adriaticimage} style={{
                    "maxWidth": "125%",
                    "maxHeight": "125%",
                }}/></div>
            </div>
            <div className="insurance">
                <h3>Garancija 12 mjeseci</h3>
                <p>Za svako naše kupljeno vozilo dobivate potvrdu i dokaz o kilometraži te o porijeklu vozila. No, ako se želite dodatno osigurati od nadolazećih troškova za održavanje rabljenog vozila naš partner nudi opciju produljenog jamstva za rabljena vozila čija je tvornička garancija istekla. Na stranici Motive Service http://hr.motiveservice.eu, možete dobiti sve informacije o usluzi produljenog jamstva, uvjetima suradnje te općenito na koje dijelove se garancija odnosi.</p>
                <div><img src={laptop} style={{
                    "maxWidth": "125%",
                    "maxHeight": "125%",
                }}/></div>
            </div>
            <div className="ransom">
                <h3>Otkup rabljenih vozila</h3>
                <p>Imate rabljeni automobil kojeg bi htjeli prodati odnosno zamijeniti za neki drugi? Dajemo mogućnost otkupa vašeg vozila, bez obzira da li je u vlasništvu privatne ili pravne osobe. Molim Vas da popunite obrazac u rubrici “kontakt” kako bismo dogovorili termin.</p>
                <div><img src={golfovi} style={{
                    "maxWidth": "125%",
                    "maxHeight": "125%",
                }}/></div>
            </div>
        </div>
    );
}

export default  Services