import React, {useContext} from 'react'
import AdminCtx from "../../../../../contexts/AdminCtx";
import { Button } from 'reactstrap';
import  { navigate } from 'hookrouter';
import {CarPreviewDto} from "../../../../../types/CarPreviewDto";
import {IoLogoEuro} from 'react-icons/io'
import CurrencyContext from "../../../../../contexts/CurrencyContext";


type Props = {car: CarPreviewDto, func:() => any}

const Car:React.FC<Props> = (props) => {

    const currencyCtx = useContext(CurrencyContext)
    const adminCtx = useContext(AdminCtx)

    return(
        <div className="singleCar" style={(adminCtx.authenticated && props.car.featured && {backgroundColor:"yellow"}) || {}}>
            {adminCtx.authenticated && <li><Button color ="primary" onClick={() => {navigate('/admin/edit/' + props.car.id)}}>Edit</Button></li>}

            <div className="singleCarProfilePic"
                 style={{backgroundImage:'url('+process.env.REACT_APP_API_URL+'/images/'+props.car.profilePicture+')'}}
                 onClick={props.func}>
            </div>
            <div className="singleCarBasicInfo">
                <div className="firstRow">
                    <ul className="infoList">
                        <li><b>Marka:</b> {props.car.make}</li>
                        <li><b>Model:</b> {props.car.model}</li>
                        <li><b>Tip:</b> {props.car.type}</li>
                    </ul>
                </div>
                <div className="secondRow">
                    <ul className="infoList">
                        <li><b>Cijena:</b>
                            {currencyCtx.currency==="EUR"? (<> {props.car.price}<IoLogoEuro/></>) : null}
                            {currencyCtx.currency==="HRK"? (<>~{Math.round(props.car.price * 7.44)}<b>kn</b></>) : null}
                        </li>
                        <li><b>Km:</b> {props.car.mileage}</li>
                        <li><b>Godina:</b> {props.car.productionYear}.</li>
                    </ul>
                </div>
            </div>           
        </div>
    );
}

export default Car