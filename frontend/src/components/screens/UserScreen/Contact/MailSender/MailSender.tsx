import React from 'react'
import {Formik} from "formik";
import {sendEmail} from "../../../../../api";
import {navigate} from "hookrouter";
import {Alert, Button} from "reactstrap";
import * as Yup from "yup";

const MailSender:React.FC = () => {

    function handleSendMail(val:any,bag:any){
        bag.setSubmitting(true)
        sendEmail(val).then(response=>(alert('Email sent!'),navigate('/')))
    }

    return(
        <>
            <Formik
                initialValues={{
                    name:'',
                    surname:'',
                    phone:'',
                    email:'',
                    subject:'',
                    text:''
                }}
                onSubmit={handleSendMail}
                validationSchema={
                    Yup.object().shape({
                        name: Yup.string().required('Required'),
                        surname: Yup.string().required('Required'),
                        phone: Yup.string().required('Required'),
                        email: Yup.string().email().required('Required'),
                        subject: Yup.string().required('Required'),
                        text: Yup.string().required('Required')
                    })
                }
                render={({
                             errors,
                             touched,
                             values,
                             handleChange,
                             handleBlur,
                             handleSubmit,
                             isSubmitting,
                         })=>(
                    <form onSubmit={handleSubmit} className="newCarForm">
                        <ul className="emailList">
                            <li>
                                <input
                                    type="text"
                                    name="name"
                                    placeholder="Ime"
                                    value={values.name}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                            </li>
                            <li>{errors.name && touched.name && <Alert color="danger">{errors.name}</Alert>}</li>
                            <li>
                                <input
                                    type="text"
                                    name="surname"
                                    placeholder="Prezime"
                                    value={values.surname}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                            </li>
                            <li>{errors.surname && touched.surname && <Alert color="danger">{errors.surname}</Alert>}</li>
                            <li>
                                <input
                                    type="text"
                                    name="phone"
                                    placeholder="Broj mobitela"
                                    value={values.phone}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                            </li>
                            <li>{errors.phone && touched.phone && <Alert color="danger">{errors.phone}</Alert>}</li>
                            <li>
                                <input
                                    type="email"
                                    name="email"
                                    placeholder="Email"
                                    value={values.email}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                            </li>
                            <li>{errors.email && touched.email && <Alert color="danger">{errors.email}</Alert>}</li>
                            <li>
                                <input
                                    type="text"
                                    name="subject"
                                    placeholder="Predmet"
                                    value={values.subject}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                            </li>
                            <li>{errors.subject && touched.subject && <Alert color="danger">{errors.subject}</Alert>}</li>
                            <li>
                                <textarea
                                    style={{width:"100%"}}
                                    rows={10}
                                    name="text"
                                    placeholder=""
                                    value={values.text}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                            </li>
                            <li>{errors.text && touched.text && <Alert color="danger">{errors.text}</Alert>}</li>
                            <li>
                                <Button type="submit" color="success" disabled={isSubmitting}>
                                    Pošalji
                                </Button>
                            </li>
                        </ul>
                    </form>
                )}
            />
        </>
    );
}

export default MailSender