import React, {useContext, useState} from 'react'
import {Alert, Button, Container} from 'reactstrap'
import {Formik} from "formik";
import AdminCtx from "../../../../contexts/AdminCtx";
import {handleAuth} from "../../../../api";
import {navigate} from "hookrouter";

const Auth:React.FC = () =>{

    const [failed, setFailed] = useState<boolean>(false)

    const adminCtx = useContext(AdminCtx)

    function handleAuthenticate(val: {username: string, password: string}, bag: any) {
        bag.setSubmitting(true)
        handleAuth(val)
            .then((response) => {
                bag.setSubmitting(false);
                if (response === true){
                    adminCtx.updateAuthenticated(response);
                    localStorage.setItem('admin','true')
                    navigate('/auti')
                } else {
                    setFailed(true)
                }
            })
    }

    return(
        <div style={{height:"83vh"}}>
            <Formik
                initialValues={{
                    username: '',
                    password: ''
                }}
                onSubmit={(values, { setSubmitting }) => {handleAuthenticate(values,{setSubmitting})}}
                render={({
                             values,
                             handleChange,
                             handleBlur,
                             handleSubmit,
                             isSubmitting,
                         }) => (
                    <form onSubmit={handleSubmit} className="authForm">
                        <ul>
                            <li>
                                <input
                                    type="username"
                                    name="username"
                                    placeholder="username"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.username}
                                />
                            </li>
                            <li>
                                <input
                                    type="password"
                                    name="password"
                                    placeholder="password"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.password}
                                />
                            </li>
                            <li>
                                <Button type="submit" color="success" disabled={isSubmitting}>
                                    Login
                                </Button>
                            </li>
                            <li>
                                {failed && <Alert color="danger">Wrong username or password. Please try again</Alert>}
                            </li>
                        </ul>
                    </form>
                )}
            />
        </div>
    );
}

export default Auth