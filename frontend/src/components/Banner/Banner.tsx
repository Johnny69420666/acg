import React from 'react';
import {IoIosPin, IoMdCall, IoMdTime} from "react-icons/io";
import acglogo from "../../acglogo.svg";

const Banner:React.FC = () => {
    return(
        <>
            <div className="infoBanner">
                <div className="bannerContact">
                    <div className="bannerContent">
                        <div style={{color:"#cccccc"}}><IoMdCall size="4em"/></div>
                        <div className="bannerContentList">
                            <ul>
                                <li><b>Kontakt:</b></li>
                                <li>Mob: 099 2320 300</li>
                                <li>Mail: acgrubisic@gmail.com</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="bannerTime">
                    <div className="bannerContent">
                        <div style={{color:"#cccccc"}}><IoMdTime size="4em"/></div>
                        <div className="bannerContentList">
                            <ul>
                                <li><b>Radno vrijeme:</b></li>
                                <li>Pon-Pet: 9:00-18:00</li>
                                <li>Sub: 9:00-14:00</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="bannerAddress">
                    <div className="bannerContent">
                        <div style={{color:"#cccccc"}}><IoIosPin size="4em"/></div>
                        <div className="bannerContentList">
                            <ul>
                                <li><b>Adresa:</b></li>
                                <li>Sobolski put 26</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="bannerLogo"><img src={acglogo} alt="asdf" style={{maxWidth:"80%",maxHeight:"60%"}}/></div>
            </div>
        </>
    );
}

export default Banner