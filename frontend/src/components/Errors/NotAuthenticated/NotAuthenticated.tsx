import React from 'react'

const NotAuthenticated:React.FC = () => {
    return(
        <>
            <div style={{minHeight:"80vh"}}>
                You have to be logged in to view this content.
            </div>
        </>
    );
}

export default NotAuthenticated